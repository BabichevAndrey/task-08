package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	private String currentTagName;

	private static final String TAG_NAME_NAME = "name";
	private static final String TAG_NAME_SOIL = "soil";
	private static final String TAG_NAME_ORIGIN = "origin";
	private static final String TAG_NAME_MULTIPLYING = "multiplying";

	public List<Flower> flowerListSax = new ArrayList<>();
	Flower flowerSax;


	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentTagName = qName;
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
	//	System.out.println("END ELEMENT "+qName);
		if (qName == "flower"){
			flowerListSax.add(flowerSax);
		}
		currentTagName = null;

	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (currentTagName == null){
			return;
		}

		String str = "";
		for (int i = 0; i < length; i++) {
			str += ch[start+i];
		}

		if (currentTagName == "flower"){
			createNewFlower();
		}

		if (currentTagName == TAG_NAME_NAME){
			flowerSax.setName(str);
		}
		if (currentTagName == TAG_NAME_SOIL){
			flowerSax.setSoil(str);
		}
		if (currentTagName == TAG_NAME_ORIGIN){
			flowerSax.setOrigin(str);
		}
		if (currentTagName == TAG_NAME_MULTIPLYING){
			flowerSax.setMultiplying(str);
		}

	}

	private void createNewFlower() {
		this.flowerSax = new Flower();
	}



	//	@Override
//	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//		super.startElement(uri, localName, qName, attributes);
////		System.out.println("URI ///////" + uri);
////		System.out.println("LocalNAME ///////" + localName);
//		System.out.println("gName //////" + qName);
////		System.out.println("ATTRIBUTS /////" + attributes);
//		curName = qName;
//	}
//
//	@Override
//	public void characters(char[] ch, int start, int length) throws SAXException {
////		System.out.println(ch);
//
//		System.out.println("Content of " + curName +" is => "+ new String(ch, start, length));
//	}
}