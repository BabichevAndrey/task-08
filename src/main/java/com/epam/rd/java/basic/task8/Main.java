package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.*;
import javax.xml.stream.*;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.util.EventReaderDelegate;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE

//		Root root = new Root();

		DocumentBuilderFactory dbf =  DocumentBuilderFactory.newInstance();
		Document doc = dbf.newDocumentBuilder().parse(new File("input.xml"));

		Node rootNode = doc.getFirstChild();
		//System.out.println("ROOT NODE IS "+rootNode.getNodeName());

		NodeList nodeList = rootNode.getChildNodes();
		Node flower = null;

		List<Flower> flowerList = new ArrayList<>();
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getNodeType() != Node.ELEMENT_NODE) {continue;}

			switch (nodeList.item(i).getNodeName()) {
				case "flower": {
					Flower flower1 = new Flower();

					flower = nodeList.item(i);
					NodeList ifFlower = flower.getChildNodes();

					for (int j = 0; j < ifFlower.getLength(); j++) {
						if (ifFlower.item(j).getNodeType() != Node.ELEMENT_NODE) {continue;}

						switch (ifFlower.item(j).getNodeName()){
							case "name":{
								flower1.setName(ifFlower.item(j).getTextContent());
							}case "soil":{
								flower1.setSoil(ifFlower.item(j).getTextContent());
							}case "origin":{
								flower1.setOrigin(ifFlower.item(j).getTextContent());
							}case "multiplying":{
								flower1.setMultiplying(ifFlower.item(j).getTextContent());
							}

						}
					}
					flowerList.add(flower1);
				}
			}
		}

		// sort (case 1)
		// PLACE YOUR CODE HERE
		Collections.sort(flowerList);
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();
		Element rootOut = document.createElement("flowers");

		document.appendChild(rootOut);
		rootOut.setAttribute("xmlns", "http://www.nure.ua");
		rootOut.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		rootOut.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

		for (Flower flower2: flowerList) {
			Element rootFlower = document.createElement("flower");
			Element rootName = document.createElement("name");
			Text textName = document.createTextNode(flower2.getName());
			Element rootSoil = document.createElement("soil");
			Text textSoil = document.createTextNode(flower2.getSoil());
			Element rootOrigin = document.createElement("origin");
			Text textOrigin = document.createTextNode(flower2.getOrigin());
			Element rootMulti = document.createElement("multiplying");
			Text textMulti = document.createTextNode(flower2.getMultiplying());

			rootOut.appendChild(rootFlower);
			rootFlower.appendChild(rootName);
			rootName.appendChild(textName);
			rootFlower.appendChild(rootSoil);
			rootSoil.appendChild(textSoil);
			rootFlower.appendChild(rootOrigin);
			rootOrigin.appendChild(textOrigin);
			rootFlower.appendChild(rootMulti);
			rootMulti.appendChild(textMulti);
		}

		Transformer t = TransformerFactory.newInstance().newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");
		t.transform(new DOMSource(document), new StreamResult(new FileOutputStream(outputXmlFile)));

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);

		SAXController handler = new SAXController("input.xml");

		SAXParserFactory factorySax = SAXParserFactory.newInstance();
		SAXParser saxParser = factorySax.newSAXParser();
		saxParser.parse(new File("input.xml"), handler);

		List<Flower> flowerListSax = handler.flowerListSax;
		Collections.sort(flowerListSax);

		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";

		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
		XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(new FileOutputStream(outputXmlFile));
		xmlStreamWriter.writeStartDocument();
		xmlStreamWriter.writeStartElement("flowers");

		xmlStreamWriter.writeAttribute("xmlns", "http://www.nure.ua");
		xmlStreamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		xmlStreamWriter.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");


		for (Flower flowerSax: flowerListSax) {
			xmlStreamWriter.writeStartElement("flower");
				xmlStreamWriter.writeStartElement("name");
				xmlStreamWriter.writeCharacters(flowerSax.getName());
				xmlStreamWriter.writeEndElement();
				xmlStreamWriter.writeStartElement("soil");
				xmlStreamWriter.writeCharacters(flowerSax.getSoil());
				xmlStreamWriter.writeEndElement();
				xmlStreamWriter.writeStartElement("origin");
				xmlStreamWriter.writeCharacters(flowerSax.getOrigin());
				xmlStreamWriter.writeEndElement();
				xmlStreamWriter.writeStartElement("multiplying");
				xmlStreamWriter.writeCharacters(flowerSax.getMultiplying());
				xmlStreamWriter.writeEndElement();
			xmlStreamWriter.writeEndElement();
//			xmlStreamWriter.writeEndDocument();
		}

		xmlStreamWriter.writeEndElement();
		xmlStreamWriter.writeEndDocument();

//		Transformer t1 = TransformerFactory.newInstance().newTransformer();
//		t1.setOutputProperty(OutputKeys.INDENT, "yes");
//		t1.transform(new DOMSource(document), new StreamResult(new FileOutputStream(outputXmlFile)));

		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLStreamReader streamReader =  inputFactory.createXMLStreamReader(new FileInputStream("input.xml"));

		List<Flower> flowerListStax = staxController.flowerListSax;
		Collections.sort(flowerListStax);

		outputXmlFile = "output.stax.xml";
		XMLOutputFactory output =  XMLOutputFactory.newInstance();
		XMLStreamWriter writer = output.createXMLStreamWriter(new FileOutputStream(outputXmlFile));

		writer.writeStartDocument();
		writer.writeStartElement("flowers");
		writer.writeAttribute("xmlns", "http://www.nure.ua");
		writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");


		for (Flower flowerSax: flowerListSax) {
			writer.writeStartElement("flower");
				writer.writeStartElement("name");
				writer.writeCharacters(flowerSax.getName());
				writer.writeEndElement();
				writer.writeStartElement("soil");
				writer.writeCharacters(flowerSax.getSoil());
				writer.writeEndElement();
				writer.writeStartElement("origin");
				writer.writeCharacters(flowerSax.getOrigin());
				writer.writeEndElement();
				writer.writeStartElement("multiplying");
				writer.writeCharacters(flowerSax.getMultiplying());
				writer.writeEndElement();
			writer.writeEndElement();
		}

		writer.writeEndElement();
		writer.writeEndDocument();

		while (streamReader.hasNext()){
			int event = streamReader.next();
			if (event == XMLStreamConstants.START_ELEMENT){
//				if (streamReader.getLocalName() == "flower"){
//					staxController.createNewFlower();
//
//				}

//				if (streamReader.getLocalName() == "name"){
//					staxController.flowerStax.setName(streamReader.getElementText());
//				}
//				if (streamReader.getLocalName() == "soil"){
//					staxController.flowerStax.setSoil(streamReader.getElementText());
//				}
//				if (streamReader.getLocalName() == "origin"){
//					staxController.flowerStax.setOrigin(streamReader.getElementText());
//				}
//				if (streamReader.getLocalName() == "multiplying"){
//					staxController.flowerStax.setMultiplying(streamReader.getElementText());
//				//	System.out.println(streamReader.getElementText());
//				}




			}
		}

//		List<Flower> flowerListStax = staxController.flowerListSTAX;
//
//		for (Flower flowerStax:
//			 flowerListStax) {
//			System.out.println(flowerStax.getName());
//
//		}

//		XMLStreamReader reader = inputFactory.createXMLEventReader(new FileInputStream("input.xml"));
//
//		while (reader.hasNext()){
//			XMLEvent event = reader.next();
//			System.out.println(event);
//		}

//		XMLEventReader reader = inputFactory.createXMLEventReader(new FileInputStream("input.xml"));
//
//
//
//		while (reader.hasNext()){
//			XMLEvent event = reader.nextEvent();
//	//		System.out.println(event);
//			if (event.isStartElement()){
//				StartElement sElement = event.asStartElement();
//				QName name = sElement.getName();
//				System.out.println(name);
//			}
//		}

		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save

		// PLACE YOUR CODE HERE
	}

}

//class Root {
////	private String flowers;
//	private List<Flower> flowers = new ArrayList<>();
//
//	public Root(List<Flower> flowers) {
//		this.flowers = flowers;
//	}
//
//	public Root() {
//
//	}
//
//	public List<Flower> getFlowers() {
//		return flowers;
//	}
//
//	public void setFlowers(List<Flower> flowers) {
//		this.flowers = flowers;
//	}
//
//	@Override
//	public String toString() {
//		return "Root{" +
//				"flowers=" + flowers +
//				'}';
//	}
//}

