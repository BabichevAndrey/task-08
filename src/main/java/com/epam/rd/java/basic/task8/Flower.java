package com.epam.rd.java.basic.task8;

public class Flower implements Comparable<Flower> {
    private String name;
    private String soil;
    private String origin;
    private String multiplying;

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public int compareTo(Flower o) {
        return this.name.compareTo(o.name);
    }
}
